# =======================================================================
# This Script can help you rename all your Python-scripts in a folder
# to contain a certrain version number in their filenames.
# It also renames the imports inside the files, you probably rely on.

# Example:
#   - import x => (Refactoring to version 1.0) =>
#     import x_1_0 as x

#   - import y as someTitle => (Refactoring to version 1.0) =>
#     import y_1_0 as someTitle

# Make sure, all your modules have their own line and are
# within the 'Own modules' block,
# starting with
# # Own...
# and ending with
# # -...
# (replace '...' with whatever you want)
# =======================================================================

import os

# Own modules
# import bacon
# import eggs as breakfast
# ------------


if __name__ == '__main__':
    suffix = '_' + input('Enter new Version: ').replace('.', '_')

    directory = os.path.dirname(os.path.realpath(__file__))
    files = os.listdir(directory)
    for f in files:
        if not f.endswith('.py') or f.endswith(suffix + '.py') or f.endswith('VersionRefactorer.py'):
            continue

        oldName = f
        newName = f[:-3] + suffix + '.py'
        print(newName)
        os.system('cp {} {}'.format(oldName, newName))

        newLines = ()
        with open(newName, 'r') as file:
            replacing = False
            for line in file.readlines():
                l = line.rstrip()
                if replacing:
                    if line.startswith('import'):
                        parts = l.split(' ')
                        l = parts[0] + ' ' + parts[1] + \
                            suffix + ' as ' + parts[-1]

                        print(l)

                if l.startswith('# Own'):
                    replacing = True
                elif l.startswith('# -'):
                    replacing = False

                l += '\n'
                newLines += (l, )

        with open(newName, 'w') as file:
            file.writelines(newLines)
