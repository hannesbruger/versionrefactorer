This Script can help you rename all your Python-scripts in a folder
to contain a certrain version number in their filenames.
It also renames the imports inside the files, you probably rely on.

Example:
- `someFile.py` => (Refactoring to version 1.0) =>
    `someFile_1_0.py`
- `import x` => (Refactoring to version 1.0) =>
    `import x_1_0 as x`

- `import y as someTitle` => (Refactoring to version 1.0) =>
    `import y_1_0 as someTitle`

Make sure, all your modules have their own line and are within the 'Own modules' block, starting with `# Own...` and ending with `# -...`
*(replace `'...'` with whatever you want)*.

Example:
```python
# Own modules
import test1
import test2 as t2

import test3 as otherTitle
# ----
```

Execution:
1. Move this file to the same folder, where your project scripts are located
2. Make sure, that the folder only contains relevant python-files for your project and no previous versions 
3. Execute this script with python 3.x (tested with 3.5.2)
5. When asked for a version number, enter any string you like ('New-Version', '1.4', '3.5b'), '.' will be replaced by '_'.
4. Move the new files to another location